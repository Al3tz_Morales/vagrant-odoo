Vagrant setup for Odoo
###########################

This vagrant setup provides you with a ready to be used Odoo installation.

What's included?
-----------------

* Ubuntu Trusty 64
* Postgresql
* Odoo

How to use it?
---------------

1. Clone the repo
2. Run `vagrant up --provider=libvirt or vagrant up --provider=virtualbox`
3. Open http://localhost:8000 in your browser

You can add additional addons under the `addons/` directory.

How does it work?
------------------

The installation is provided by puppet scripts.

1. installs some basic packages
2. installs postgresql
3. installs Odoo by

  * downloading the latest nightly build (.deb file)
  * installing required packaged
  * installing the debian package
  * overwriting the default config file
  * restarting the service

How to customize?
------------------

You can customize it in any way you want, but there are a few simple tweaks.

* `puppet/vagrant.pp` defines the `$OPENERP_VERSION` variable that will be downloaded and installed
* the `addons` directory can contain custom OpenERP addons

NFS Export
----------------
Gentoo/Sabayon permissions on directory/files

If you don't have problems in export directories or files trought nfs,
place the files do you want export in "/var/www".
http://nfs.sourceforge.net/nfs-howto/ar01s07.html#pemission_issues
list all groups available: getent group
change gid sudo groupmod -g 1001 vagrant

After vagrant modified "/etc/exports", you run manually sudo exportfs -ra, to sync
resources exported.
Run nfs server on gentoo with systemd
sudo systemctl start rpcbind.service
sudo systemctl start nfsd.service
sudo syetmctl enable rpc-mountd.service

sudo modprobe -iv nfsd

 sudo -u postgres psql
 alter user openerp with password 'openerp';
Error:
Use the same encoding as in the template database, or use template0 as template.
 https://gist.github.com/ffmike/877447
 http://jacobian.org/writing/pg-encoding-ubuntu/

In Gentoo
 Edit /etc/conf.d/nfs and change this lines
 OPTS_RPC_MOUNTD="-p 32767"
 OPTS_RPC_STATD="-p 32765 -o 32766"
 Exec rpcinfo -p, to check ports of portmapper, nfs, mountd
 Add rules to firewall
 sudo ufw allow from 192.168.56.0/24 to any port 111
 sudo ufw allow from 192.168.56.0/24 to any port 2049
 sudo ufw allow from 192.168.56.0/24 to any port 32767
