class consejosano{
    case $operatingsystem {
        ubuntu: {
            exec {
                "pip install beautifulsoup4":
                command => "sudo pip install beautifulsoup4",
            }
            exec {
                "pip install requests_oauthlib":
                command => "sudo pip install requests_oauthlib",
            }
            exec{
                "pip install twilio":
                command => "sudo pip install twilio",
            }
            exec{
                "pip install backports.pbkdf2":
                command => "sudo pip install backports.pbkdf2",
            }
            exec{
                "pip install infusionsoft":
                command => "sudo pip install git+https://github.com/infusionsoft/Official-API-Python-Library.git",
            }
        }
    }
}
