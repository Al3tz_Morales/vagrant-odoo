# Custom installation
class custom {
    case $operatingsystem {
        ubuntu: {
            exec {
                "apt-get install byobu":
                command => "sudo apt-get install byobu -y",
            }
            exec {
                "apt-get install htop":
                command => "sudo apt-get install htop -y",
            }
            exec {
                "pip winpdb":
                command => "sudo pip install winpdb",
            }
            exec{
                "pip ipython":
                command => "sudo pip install ipython",
            }
            exec{
                "pip erppeek":
                command => "sudo pip install ERPpeek"
            }
            exec{
                "pil, pillow jpg support":
                command => "sudo apt-get install libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev libpng12-dev -y"
            }
            exec{
                "pip pillow":
                command => "sudo pip install pillow"
            }
            # exec {
            #     "login on psql console with user postgres":
            #     command => "sudo -u postgres psql \
            #     alter user openerp with password 'openerp';
            #     \\q",
            # }
            # exec {
            #     "change password for user openerp":
            #     command => "alter user openerp with password 'openerp';",
            # }
            # exec {
            #     "exit psql console":
            #     command => "\\q",
            # }
        }
    }
}
