# Odoo installation
class odoo {
    # exec {
    #             "apt-get install pip":
    #             command => "sudo apt-get install pip -y",
    #         }
    package { ["python-decorator","python-imaging","python-passlib","python-pypdf","python-dateutil", "python-feedparser", "python-gdata", "python-ldap", "python-libxslt1", "python-lxml", "python-mako", "python-openid", "python-psycopg2", "python-pybabel", "python-pychart", "python-pydot", "python-pyparsing", "python-reportlab", "python-simplejson", "python-tz", "python-vatnumber", "python-vobject", "python-webdav", "python-werkzeug", "python-xlwt", "python-yaml", "python-zsi", "python-docutils", "python-jinja2", "python-mock", "python-psutil", "python-unittest2", "wkhtmltopdf"]:
        ensure => installed,
    }
    # requirement for odoo execution
    exec { "pip install QUnitSuite":
        command => "sudo pip install QUnitSuite",
    }
    exec { "wget-odoo":
      command => "wget http://nightly.odoo.com/$OPENERP_VERSION/nightly/deb/odoo_$OPENERP_VERSION.latest_all.deb",
      require => [ Package["python-decorator","python-imaging","python-passlib","python-pypdf","python-dateutil", "python-feedparser", "python-gdata", "python-ldap", "python-libxslt1", "python-lxml", "python-mako", "python-openid", "python-psycopg2", "python-pybabel", "python-pychart", "python-pydot", "python-pyparsing", "python-reportlab", "python-simplejson", "python-tz", "python-vatnumber", "python-vobject", "python-webdav", "python-werkzeug", "python-xlwt", "python-yaml", "python-zsi"] ]
    }
    exec { "apt-install-odoo":
    	command => "sudo dpkg -i odoo_$OPENERP_VERSION.latest_all.deb",
        require => Exec["wget-odoo"]
    }
    file { "/etc/odoo/openerp-server.conf":
        source => "/vagrant/puppet/files/etc/odoo/openerp-server.conf",
        owner => "root",
        group => "root",
        mode => 0644,
        ensure => file,
        require => Exec["apt-install-odoo"]
    }
    exec { "restart-odoo":
        command => "service odoo restart",
        require => File["/etc/odoo/openerp-server.conf"]
    }
}
